import styles from '../styles/Footer.module.css'
import Image from 'next/image'
import Link from 'next/link'

export default function Footer() {
    return (
        <footer className={styles.footer}>
          <a
            href="https://irando.co.id"
            target="_blank"
            rel="noopener noreferrer"
          >
            Developed by{' '}
            <span className={styles.logo}>
              Robert Nicjoo
              {/* <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} /> */}
            </span>
          </a>
        </footer>
    );
}