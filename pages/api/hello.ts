import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  name: string,
  username: string,
  email: string,
  dob: string,
  address: string,
  phone: string,
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.status(200).json({
    name: 'Alex Thompson',
    username: 'babyblue',
    email: 'alex.thompson@example.com',
    dob: '10/6/1988',
    address: '4083 Plum St',
    phone: '(623) 461-2357',
  })
}
